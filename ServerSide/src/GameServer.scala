import java.io.PrintWriter
import java.net.ServerSocket
import java.util.{Date, Locale}
import java.text.DateFormat
import java.text.DateFormat._


/**
 * A server for a network multi-player tic tac toe game.  Modified and
 * extended from the class presented in Deitel and Deitel "Java How to
 * Program" book.  I made a bunch of enhancements and rewrote large sections
 * of the code.  The main change is instead of passing *data* between the
 * client and server, I made a TTTP (tic tac toe protocol) which is totally
 * plain text, so you can test the game with Telnet (always a good idea.)
 * The strings that are sent in TTTP are:
 *
 * Client -> Server           Server -> Client
 * ----------------           ----------------
 * MOVE <n>  (0 <= n <= 8)    WELCOME <char>  (char in {X, O})
 * QUIT                       VALID_MOVE
 * OTHER_PLAYER_MOVED <n>
 * VICTORY
 * DEFEAT
 * TIE
 * MESSAGE <text>
 *
 * A second change is that it allows an unlimited number of pairs of
 * players to play.
 */

object GameServer {

  def main(args: Array[String]): Unit = {
    val port = 9000
    InitializeTicTacToe(port)
  }

  def InitializeTicTacToe(port: Int): Unit = {

    val listener = new ServerSocket(port)
    println("Tic Tac Toe Server is running")

    try {
      while (true) {
        val game = new Game(listener.accept(), listener.accept())
        game.StartGame()
      }
    } catch {
      case e: Exception => {
        println(e.getMessage)
      }
    }
  }
}
