import java.net.Socket

/**
 * Created by xyarn on 13.03.2016.
 */
class Game(pSocket1: Socket, pSocket2: Socket) {
  val board = Array(0, 0, 0, 0, 0, 0, 0, 0, 0)

  var currentPlayer : Player = null

  def hadWinner(): Boolean = {
    (board(0) != 0 && board(0) == board(1) && board(0) == board(2)) ||
      (board(3) != 0 && board(3) == board(4) && board(3) == board(5)) ||
      (board(0) != 0 && board(0) == board(3) && board(0) == board(6)) ||
      (board(6) != 0 && board(6) == board(7) && board(6) == board(8)) ||
      (board(1) != 0 && board(1) == board(4) && board(1) == board(7)) ||
      (board(2) != 0 && board(2) == board(5) && board(2) == board(8)) ||
      (board(0) != 0 && board(0) == board(4) && board(0) == board(8)) ||
      (board(2) != 0 && board(2) == board(4) && board(2) == board(6))
  }

  def legalMove(location: Int, player: Player): Boolean = {
    this.synchronized{
      if (player == currentPlayer && board(location) == 0) {
        board(location) = currentPlayer.mark.toInt
        currentPlayer = currentPlayer.opponent
        currentPlayer.otherPlayerMoved(location)
        true
      }else{
        false
      }
    }
  }

  def boardFilledUp(): Boolean = {
    for (i <- board.indices) {
      if (board(i) == 0) {
        return false
      }
    }
    true
  }

  def StartGame(): Unit = {
    val p1 = new Player(pSocket1, 'X', this)
    val p2 = new Player(pSocket2, 'O', this)
    p1 opponent = p2
    p2 opponent = p1

    currentPlayer = p1
    val t1 = new Thread(p1)
    val t2 = new Thread(p2)
    t2 start()
    t1 start()
  }
}
