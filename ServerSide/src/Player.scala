
import java.io._
import java.net._

/**
 * Created by xyarn on 13.03.2016.
 */
class Player(socket: Socket, markArg: Char, gameArg: Game) extends Runnable {
  var mark: Char = markArg
  var game: Game = gameArg
  var opponent: Player = null
  var input = new BufferedReader(new InputStreamReader(socket getInputStream))
  var output = new PrintWriter(socket getOutputStream)

  def run(): Unit = {
    output.println("WELCOME " + mark)
    output.println("MESSAGE Waiting for opponent to connect")
    output.println("MESSAGE All players connected")
    output.flush()

    if (mark == 'X') {
      output.println("MESSAGE Your move")
      output.flush()
    }

    try {
      // The thread is only started after everyone connects.
      output.println("MESSAGE All players connected")
      output.flush()
      // Tell the first player that it is her turn.
      if (mark == 'X') {
        output.println("MESSAGE Your move")
        output.flush()
      }

      // Repeatedly get commands from the client and process them.
      while (true) {
        val command: String = input.readLine()
        println(command)
        if (command.startsWith("MOVE")) {
          val location = Integer.parseInt(command.substring(5))
          if (game.legalMove(location, this)) {
            output.println("VALID_MOVE")
            output.flush()
            for(i<- game.board.indices){
              println(game.board(i))
            }
            if (game.hadWinner()) {
              output.println("VICTORY")
            } else if (game.boardFilledUp()) {
              output.println("TIE")
            }
            output.flush()
          } else {
            output.println("MESSAGE ?")
            output.flush()
          }
        } else if (command.startsWith("QUIT")) {
          return
        }
      }
    } catch {
      case e: IOException => System.out.println("Player died: " + e)
    } finally {
      try {
        socket.close()
      } catch {
        case e: IOException => System.out.println("Player died: " + e)
      }
    }
  }


  def otherPlayerMoved(location: Int): Unit = {
    output.println("OPPONENT_MOVED " + location)
    if (game.hadWinner()) {
      output.println("DEFEAT")
    } else if (game.boardFilledUp()) {
      output.println("TIE")
    }
    output.flush()
  }
}
